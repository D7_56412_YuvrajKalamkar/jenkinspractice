//Importing modules

const express = require('express')

const app = express();

// parsing the request data

app.use(express.json())


//starting server

app.listen (4545, () =>{
    console.log ('server started at port 3360');
})